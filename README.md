# Android Basics: User Interface

## Building Layout

### Views

A **view** is a rectangle on the screen that shows some content. It should be an image, a button, a piece of text, or anything else that the app can display.

Everything about what you see and interact with in your app is called the **user interface**, or **UI** for short.

![example_android_views](images/example_android_views.PNG)



In other words, a view is the basic building block that you use to build up the layout for your app.

The format used for naming the objects is **camel case**.



#### dp

Density-Independent Pixels. [Understanding Layout](https://material.io/design/layout/understanding-layout.html#).

![density_independent_pixels](images/density_independent_pixels.PNG)



#### wrap_content

The special value `wrap_content` is used to shrink-wrap the View around its content.



#### sp

Scale-Independent Pixels. `sp` is only used for fonts because it adjusts based on the user's preferences and settings for text size. [Material Design typography guidelines](http://www.google.com/design/spec/style/typography.html#typography-styles).



#### color

See more in [Material Design color palette](http://www.google.com/design/spec/style/color.html#color-color-palette) and [Hex colors](http://www.w3schools.com/colors/colors_hex.asp).



#### Documentation

[**developer.android.com**](https://developer.android.com/)



------

### ViewGroups

**ViewGroups** are also views and they are rectangles on the screen. The content that a **ViewGroup** holds is other views. The biggest View of all -the one that contains all others- is called the **Root View**. In other words, *a ViewGroup is a container of views*.

When a view contains other views, we call it the **parent** of these views, and the views it contains are its own **children**, and they are **siblings** to each other.



#### LinearLayout

This ViewGroup can arrange children in a vertical column or in a horizontal row.

![linearlayout](https://developer.android.com/images/ui/linearlayout.png?hl=es-419 "LinearLayout")



#### RelativeLayout

This ViewGroup has its own rules for how to position its children within it. The position of each view can be specified as relative to sibling elements (such as to the left-of or below another view) or in positions relative to the parent `RelativeLayout` area (such as aligned to the bottom, left or center).

![relativelayout](https://developer.android.com/images/ui/relativelayout.png "RelativeLayout")



#### match_parent

The special value `match_parent` is used to allow the View to expand to occupy the full size of its parent.



#### Evenly Spacing Out Children Views

Taking advantage of **screen real estate** -*available space on the screen*-.



**layout_weight:** The number of shares claimed by each child is called the **layout weight** of that child.



#### Relative to Parent

The children inside a RelativeLayout can be positioned **relative to parent's edges (left, top, right and bottom)**

```xml
android:layout_alignParentTop="true" or "false"
android:layout_alignParentBottom="true" or "false"
android:layout_alignParentLeft="true" or "false"
android:layout_alignParentRight="true" or "false"
android:layout_centerHorizontal="true" or "false"
android:layout_centerVertical="true" or "false"
```



#### Relative to Other Views

The children can be positioned relative to other sibling views by adding constraints on their position.

```
android:layout_toLeftOf=<view_id>
android:layout_above=<view_id>
android:layout_toRightOf=<view_id>
android:layout_below=<view_id>
```



#### Padding vs Margin

![pading_vs_margin](images/pading_vs_margin.PNG)



##### Padding

`View` attributes:

```
android:padding="8dp"
	OR
android:paddingLeft="8dp"
android:paddingRight="8dp"
android:paddingTop="8dp"
android:paddingBottom="8dp"
```



##### Margin

`View` attributes (Need a `ViewGroup`):

```
android:layout_margin="8dp"
	OR
android:layout_marginLeft="8dp"
android:layout_marginRight="8dp"
android:layout_marginTop="8dp"
android:layout_marginBottom="8dp" 
```



### TextView Attributes

* `android:textSize` Size of the text. Recommended dimension type for text is "sp" for scaled-pixels.
* `android:fontFamily` Font family (named by string or as a font resource reference) for the text.
* `android:textColor` Color of the text.



### ImageView Attributes

* `android:src` Sets a drawable as the content of this ImageView. 
* `android:scaleType` Controls how the image should be resized or moved to match the size of this ImageView.



### Happy Birthday Project

https://github.com/udacity/Happy-Birthday



### Extras

* [Android Developers blog](http://android-developers.blogspot.com/): It's written by Google Design Advocate, Roman Nurik.

- [Styling Android blog](http://blog.stylingandroid.com/): A blog that shows off various technical aspects of building design elements of Android apps.
- [Chris Banes' blog](https://chris.banes.me/): A blog that gives you a deeper look into Android support libraries.
- [Fragmented Podcast](http://fragmentedpodcast.com/): A weekly podcast filled with Android development discussion.